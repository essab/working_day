from datetime import date, timedelta
from calendar import SATURDAY, SUNDAY



def parse(my_date):
    """Returns a date enter by the user in object date"""
    d, m, y = map(int, my_date.split("/"))
    return date(y, m, d)


def days_diff(date_1, date_2):
    """Returns th difference in day of two date"""
    return abs((parse(date_1) - (date_2)).days)


def work_days(date_1, n):
    """Return the number of the buisness day"""
    somme = 0
    info = parse(date_1)
    for n in range(n):
        days = info + timedelta(days=n)
        if days.weekday() == SATURDAY or days.weekday() == SUNDAY:
            somme += 1
    return n - somme


def is_number(s):
    """Verify that a string of characters is an integer"""
    try:
        float(s)
        return True
    except ValueError:
        return False


def correct_date(A):
    """Check that a date in a correct format"""
    b = "/"
    if A[2] == b and A[5] == b and is_number(A[0:2]) and is_number(A[3:5]) and is_number(A[6:]):
        return True
    else:
        return False


previous_date = "00;00;00"
while not correct_date(previous_date):
    previous_date = input(" Entrez une date au format jj/MM/AAAA:")
day_off = int(input("Entrez vos conges:"))
# Recovered and check a date in a correct format

n_days = days_diff(previous_date, date.today())
days_working = work_days(previous_date, n_days)
buisness_day = days_working - day_off
# Performs the caclulation of the day working

print(buisness_day)
